from django.shortcuts import render,HttpResponseRedirect
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from .models import Post



def post_list(request):
    posts = Post.objects.all().order_by('published_date')
    return render(request, 'blog/post_list.html', {"posts":posts})


@login_required(login_url="/admin/login/")
def delete_post(request,post_id):
    post = Post.objects.get(id=post_id)
    post.delete()
    return HttpResponseRedirect("/")

@login_required(login_url="/admin/login/")
def edit_post(request,post_id):
    post = Post.objects.get(id=post_id)
    if request.POST:
        title=request.POST.get("title")
        text=request.POST.get("text")
        post.text=text
        post.title=title
        post.save()
        return HttpResponseRedirect("/")
    else:
        return render(request,'blog/editpost.html',{"post":post})
     
    
@login_required(login_url="/admin/login/")
def create_post(request):
    if request.POST:
        title=request.POST.get("title")
        text=request.POST.get("text")
        post=Post(author=request.user,title=title,text=text)
        post.save()
        return HttpResponseRedirect("/")
    else:
        return render(request,'blog/editpost.html',{})
     
